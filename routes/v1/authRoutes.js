const { Router } = require('express');
const express = require('express')
const router = express.Router();

const {login} = require('../../controllers/v1/authController')

router.post("/login", login)

module.exports = router