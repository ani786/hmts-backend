const bcrypt = require('bcrypt');

const SALTROUNDS = 10

module.exports.hashPassword = (password) => {
	return new Promise(async (resolve, reject) => {
		try{
			let hashed = await bcrypt.hash(password, SALTROUNDS)
			resolve(hashed)
		} catch(error) {
			response = {
				error: true,
				errorMessage: "Failed to hash password"
			}
			reject(response)
		}
	})
}