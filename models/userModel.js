const mongoose = require("mongoose");
const encrypt = require("mongoose-encryption");

const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	name: String,
	relativeName: String,
	relativeNumber: String,
	address: String,
	gender: String,
	email: String,
	device: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Device"
	}
})

// const encKey = process.env.ENC_KEY;
// const sigKey = process.env.SIG_KEY;

// userSchema.plugin(encrypt, { encryptionKey: encKey,  signingKey: sigKey } )

module.exports = mongoose.model('User', userSchema)