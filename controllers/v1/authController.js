const UserModel = require('../../models/userModel')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

module.exports.login = async (req, res) => {
	try {
		let user = await UserModel.findOne({username: req.body.username}).exec();
		if(!user) {
			throw "Invalid username or password"
		}
		let match = await bcrypt.compare(req.body.password, user.password)
		if(!match) {
			throw "Invalid username or password"
		}
		let jwtPayload = {
			id: user._id
		}
		let token = await jwt.sign(jwtPayload, process.env.JWTSECRET, {expiresIn:"7d"})
		res.status(200).json({...user._doc, token});
	} catch(error) {
		response = {
			error: true,
			errorMessage: error
		}
		res.status(500).json(response)
	}
}