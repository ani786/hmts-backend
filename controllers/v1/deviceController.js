const Device = require("../../models/deviceModel");
const User = require("../../models/userModel");
const cryptoRandomString = require('crypto-random-string')

module.exports.verifyDevice = async (req, res) => {
	let { id } = req.params;

	try {
		let device = await Device.findOne({ deviceID: id }).exec();

		if (!device) throw "Device not found";

		res.status(200).json({ exist: true });
	} catch (error) {
		let response = {
			exist: false,
		};
		res.status(404).json(response);
	}
};

module.exports.getDeviceData = async (req, res) => {
	try {
		let userID = req.user.id;
		let user = await User.findById(userID).populate("device").exec();
		let device = user.device;
		res.status(200).json(device);
	} catch (error) {
		let response = {
			error: true,
			message: "Something went wrong",
		};
		res.status(500).json(response);
	}
};

module.exports.updateDevice = async (req, res) => {
	try {
		const { id } = req.params;
		const { temp, pulse, lat, lon } = req.body;
		const device = await Device.findOne({ deviceID: id }).exec();
		if (!device) throw "Not found";
		device.temp = temp;
		device.pulse = pulse;
		device.lat = lat;
		device.lon = lon;
		let updatedDevice = await device.save();
		res.status(200).json({
			success: true,
			device: updatedDevice,
		});
	} catch {
		res.status(500).json({ success: false });
	}
};

module.exports.createRandomDevice = async (req, res) => {
	const randomNumber = (min, max) => {
		return Math.random() * (max - min) + min;
	};

	try {
		const deviceID = cryptoRandomString({ length: 6, type: "url-safe" });
		const temp = randomNumber(20, 40);
		const pulse = randomNumber(70, 100);
		const lat = randomNumber(19.177084, 19.176025);
		const lon = randomNumber(72.966761, 72.964519);
		let newDeviceData = { deviceID, temp, pulse, lat, lon };
		let device = new Device(newDeviceData);
		let saved = await device.save();
		res.status(200).json(saved);
	} catch (error) {
		let response = {
			error: true,
			errorMessage: "Something went wrong. Please try again.",
		};
		res.status(500).json(response);
	}
};
