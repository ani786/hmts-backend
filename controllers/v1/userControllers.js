const {hashPassword} = require('../../utils/utils')
const UserModel = require('../../models/userModel')
const Device = require('../../models/deviceModel')

module.exports.getSingleUser = async(req,res) => {
	try {
		let {id} = req.params
		if(id !== req.user.id) {
			throw "Something went wrong. Please try again"
		}
		let user = await UserModel.findById(id).exec()
		res.status(200).json(user)
	} catch(error) {
		let response = {
			error: true,
			errorMessage: "Something went wrong. Please try again."
		}
		res.status(500).json(response)
	}
}

module.exports.getAllUsers = async (req, res) => {
	try{
		let users = await UserModel.find({}).exec()
		res.status(200).json(users)
	} catch(error) {
		let response = {
			error: true,
			errorMessage: "Something went wrong. Please try again."
		}
		res.status(500).json(response)
	}
}

module.exports.addUser = async (req, res) => {
	try {
		let hashed = await hashPassword(req.body.password)
		let device = await Device.findOne({deviceID: req.body.device}).exec()
		let newUser = {
			...req.body,
			password: hashed,
			device: device._id
		}
		let user = new UserModel(newUser)

		let saved = await user.save()
		res.status(200).json(saved)
	} catch(error) {
		let response = {
			error: true,
			errorMessage: "Something went wrong. Please try again."
		}
		res.status(500).json(response)
	}
}

module.exports.updateUser = async (req, res) => {
	let {id} = req.params
	try {
		if(id !== req.user.id) {
			throw "Something went wrong. Please try again"
		}
		
		let user = await UserModel.findById(id).exec()

		for(field in req.body) {
			if(field == "password") {
				let hashed = await hashPassword(req.body[field])
				req.body[field] = hashed
			}

			user[field] = req.body[field]
		}

		let updatedUser = await user.save()
		res.status(200).json(updatedUser)
	} catch(error) {
		let response = {
			error: true,
			errorMessage: "Something went wrong. Please try again."
		}
		res.status(500).json(response)
	}
}

module.exports.checkUsername = async (req,res) => {
	let {username} = req.params
	let user = await UserModel.findOne({username}).exec()
	if(user) {
		res.status(200).json({available: false})
	} else {
		res.status(200).json({available: true})
	}
}